<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/8/2015
 * Time: 11:01 AM
 */

/* config db */
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PWD', '');
define('DB_NAME', 'cms.new');

/* config hostname */
define('HOST_NAME', 'http://cms.new.local');