<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 3:17 PM
 */

class Template {
    public $layout = '';
    public $data = array();
    public function __construct(){
    }

    public function __set($k, $v){
        $this->data[$k] = $v;
    }
    public function __get($k){
        return $this->data[$k];
    }

    public function render($filename, $isAdmin) {
        ob_start();
        if ($isAdmin) {
            include(PG_ROOT . '/admin/views/theme/ace/template/layout/main.phtml');
        }
        else {
            include(PG_ROOT . '/frontend/views/theme/default/template/'.$filename);
        }
        echo ob_get_clean();
    }
}