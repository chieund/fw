<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 3:17 PM
 */

class Layout {
    public $name = '';
    public $data = array();
    public function __construct(){
    }

    public function __set($k, $v){
        $this->data[$k] = $v;
    }
    public function __get($k){
        return $this->data[$k];
    }

    public function content() {
        include(PG_ROOT . '/admin/views/theme/ace/template/'.$this->name);
    }
}