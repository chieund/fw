<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 3:18 PM
 */
class Controller {
    public $data = array();
    private $registry;
    private static $instance;
    private static $isAdmin = false;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    protected function __construct($regis) {
        $this->registry = $regis;
    }

    public function __get($key) {
        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }

    public function dispatch($registry) {
        $this->registry = $registry;

        $uri = $this->request->getUrl();
        if ($uri == '/') {
            $file = PG_ROOT . '/frontend/controller/HomeController.php';
            include $file;
            $obj = new HomeController($this->registry);
            call_user_func_array(array($obj, 'index'), array());
            return false;
        }

        if (strpos($uri, 'admin') !== false) {
            $uri = str_replace('admin/', '', $uri);
            self::$isAdmin = true;
        }
        parse_str($uri, $params);

        if (!isset($params['controller'])) {

            $file = PG_ROOT . '/frontend/controller/NotFoundController.php';
            include $file;
            $obj = new NotFoundController($this->registry);
            $obj->index();
            return false;
        }

        $controller = ucfirst(strtolower($params['controller']));
        $action = $params['action'];

        $class = $controller . 'Controller';
        $frontend = self::$isAdmin ? 'admin' : 'frontend';
        $file = PG_ROOT . "/{$frontend}/controller/{$class}.php";

        if (!file_exists($file)) {
            $file = PG_ROOT . "/{$frontend}/controller/NotFoundController.php";
            include $file;
            $obj = new NotFoundController($this->registry);
            $obj->index();

            return true;
        }
        include $file;
        $obj = new $class($this->registry);
        call_user_func_array(array($obj, $action), array());
    }

    public function getParams() {
        $res = $this->request->getParams();
        unset($res['controller']);
        unset($res['action']);
        return $res;
    }

    public function render($filename) {
        /* Get layout name & data for layout */
        $this->layout->name = $filename;
        $this->layout->data = $this->data;

        /* Get layout name & data for layout */
        $this->template->layout = $this->layout;
        $this->template->render($filename, self::$isAdmin);
    }

    public function model($filemodel) {
        $frontend = self::$isAdmin ? 'admin' : 'frontend';
        $file = PG_ROOT . "/{$frontend}/model/{$filemodel}.php";
        if (!file_exists($file)) {
            return FALSE;
        }
        include $file;
    }

    public function redirect($url) {
        header("Location: {$url}");
        die();
    }

    public function url($controller, $action, $params = null) {
        $par = '';
        if (!empty($params)) {
            $par = http_build_query($params);
        }

        $par = !empty($par) ? '&' . $par : '';
        $url = sprintf(HOST_NAME . "/?admin&controller=%s&action=%s%s", $controller, $action, $par);
        return $url;
    }
}