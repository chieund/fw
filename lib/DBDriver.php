<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/8/2015
 * Time: 10:58 AM
 */
class DBDriver {
    private $_link = null;
    private static $instance;

    public function __construct() {}

    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function connectionDB() {
        $this->_link = mysql_connect(DB_HOST, DB_USERNAME, DB_PWD);
        if (!$this->_link) {
            die('Could not connection');
        }
        mysql_select_db(DB_NAME, $this->_link) or die('Could not select database.');
    }

    public function db_query($query) {
        return mysql_query($query, $this->_link);
    }

    public function closeDB() {
        mysql_close($this->_link);
    }

}