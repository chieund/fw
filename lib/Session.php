<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/9/2015
 * Time: 2:31 PM
 */
class Session {
    public $data = array();
    public function __construct() {
        if (!session_id()) {
            ini_set('session.use_only_cookies', 'On');
            ini_set('session.use_trans_sid', 'Off');
            ini_set('session.cookie_httponly', 'On');

            session_set_cookie_params(0, '/');
            session_start();
        }

        $this->data =& $_SESSION;
    }

    public function destroy() {
        session_destroy();
    }
}