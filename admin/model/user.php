<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/9/2015
 * Time: 1:56 PM
 */
class User extends Model {
    public function getAllUser() {
        global $database;

        $sql = "SELECT * FROM user";
        $result = $database->db_query($sql);

        $user = array();
        while ($row = mysql_fetch_array($result)) {
            $user[] = array(
                'id' => $row['id'],
                'username' => $row['username']
            );
        }
        return $user;
    }

    public function login($username, $password) {
        global $database;
        $sql = sprintf("SELECT id, username FROM user WHERE username= '%s' AND password = '%s'", $username, $password);
        $row = mysql_fetch_assoc($database->db_query($sql));
        return $row;
    }

    public function add($data) {
        global $database;
        $sql = sprintf("INSERT INTO user SET username = '%s', password ='%s'", $data['username'], $data['password']);
        $row = $database->db_query($sql);
        return $row;
    }

    public function edit($data, $userId) {
        global $database;
        $sql = sprintf("UPDATE user SET username = '%s', password ='%s' WHERE id=%d", $data['username'], $data['password'], $userId);
        $database->db_query($sql);
    }

    public function getUserById($userId) {
        global $database;
        $sql = sprintf("SELECT id, username, password FROM user WHERE id = %d", $userId);
        $result = $database->db_query($sql);

        $row = mysql_fetch_array($result);
        return isset($row['id']) ? $row : null;
    }

    public function delete($userId) {
        global $database;
        $sql = sprintf("DELETE FROM user WHERE id=%d LIMIT 1", $userId);
        $database->db_query($sql);
    }
}