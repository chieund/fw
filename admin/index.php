<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 3:11 PM
 */

/* Show display errors */
ini_set('display_errors', TRUE);
error_reporting(E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_USER_ERROR);

/* Get url root */
define('PG_ROOT', realpath(dirname(dirname(__FILE__))));

/* config */
include PG_ROOT . "/config.php";

/* include lib */
include PG_ROOT . "/lib/registry.php";
include PG_ROOT . "/lib/DBDriver.php";
include PG_ROOT . "/lib/Controller.php";
include PG_ROOT . "/lib/Model.php";
include PG_ROOT . "/lib/Request.php";
include PG_ROOT . "/lib/Template.php";
include PG_ROOT . "/lib/Session.php";
include PG_ROOT . "/lib/Layout.php";

$registry = new Registry();

$session = new Session();
$registry->set('session', $session);

$request = new Request();
$registry->set('request', $request);

$template = new Template();
$registry->set('template', $template);

$layout = new Layout();
$registry->set('layout', $layout);

/* db */
$database = DBDriver::getInstance();
$database->connectionDB();
$registry->set('database', $database);

$controller = Controller::getInstance();
$controller->dispatch($registry);
