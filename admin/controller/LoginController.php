<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/8/2015
 * Time: 2:23 PM
 */
class LoginController extends Controller {
    public function index() {
//        $this->session->data['abc'] = 1;
//        print_r($_SESSION);
//        print_r($this->data->get('session')->data['favcolor']);
       // $this->render('admin/login.phtml');
//        print_r($this->request);
//        print_r($this->data->get('session'));
//                print_r($this->session['data']);
        $this->render('admin/login.phtml');
    }

    public function login() {
        $this->model('User');

        $user = new User();
        $row  = $user->login($this->request->post['username'], $this->request->post['password']);
        if ($row['id']) {
            $this->session->data['username'] = $row['username'];
            $this->redirect('/?admin&controller=admin&action=index');
        }
    }
}