<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/8/2015
 * Time: 2:23 PM
 */
class AdminController extends Controller {
    public function index() {
        if (isset($this->session->data['username'])) $this->render('admin/index.phtml');
        else $this->redirect($this->url('login', 'index'));
    }

    public function grid() {
        $this->model('User');
        $obj = new User();

        $results = $obj->getAllUser();
        $users = array();
        foreach ($results as $user) {
            $users[] = array(
                'id'            => $user['id'],
                'username'      => $user['username'],
                'link_edit'     => $this->url('admin', 'edit', array('id' => $user['id'])),
                'link_delete'   => $this->url('admin', 'delete', array('id' => $user['id'])),
            );
        }

        $this->data['users'] = $users;
        $this->render('user/grid.phtml');
    }

    public function add() {
        if ($this->request->post) {
            $this->model('User');
            $obj = new User();
            $obj->add($this->request->post);
            $this->redirect($this->url('admin', 'grid'));
        }

        $this->data['url_add'] = $this->url('admin', 'add');
        $this->render('user/add.phtml');
    }

    public function edit() {
        $user_id = $this->request->get['id'];
        $this->model('User');

        print_r($this->userInfo->login('admin', 'admin'));

        if ($this->request->post) {
            $obj = new User();
            $obj->edit($this->request->post, $user_id);
            $this->redirect($this->url('admin', 'grid'));
        }

        if (!empty($user_id)) {
            $obj = new User();
            $user = $obj->getUserById($user_id);
            $this->data['user'] = $user;
        }

        $this->data['url_edit'] = $this->url('admin', 'edit', array('id' => $user_id));
        $this->render('user/edit.phtml');
    }

    public function delete() {
        $user_id = $this->request->get['id'];
        if (!empty($user_id)) {
            $this->model('User');
            $obj = new User();
            $obj->delete($user_id);
            $this->redirect($this->url('admin', 'grid'));
        }
    }
}