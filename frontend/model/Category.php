<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 5:16 PM
 */
class Category extends Model {
    public function getAllCategory() {
        global $database;

        $sql = "SELECT * FROM category";
        $result = $database->db_query($sql);

        $category = array();
        while ($row = mysql_fetch_array($result)) {
            $category[] = array(
                'id' => $row['id'],
                'name' => $row['name']
            );
        }
        return $category;
    }
}