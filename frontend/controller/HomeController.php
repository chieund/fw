<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 3:24 PM
 */

class HomeController extends Controller {
    public function index() {
        $this->model('Category');
//
        $objCategory = new Category();
        $category = $objCategory->getAllCategory();
//        $params = $this->getParams();

        $this->data['title'] = 'FW';
        $this->data['category'] = $category;
        $this->render('layout/home.phtml');
    }
}