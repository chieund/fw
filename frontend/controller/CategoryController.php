<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 5:13 PM
 */
class CategoryController extends Controller {
    public function index() {
        $this->model('Category');

        $objCategory = new Category();
        $category = $objCategory->getAllCategory();


        $this->data['category'] = $category;
        $this->render('category/index.phtml');
    }

    public function add() {
        print_r($this->request);
        print_r($this->session);
        $this->render('category/add.phtml');
    }
}