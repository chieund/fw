<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/8/2015
 * Time: 10:34 AM
 */
class NotFoundController extends Controller {
    public function __construct() {}

    public function index() {
        $this->render('notfound/index.phtml');
    }
}