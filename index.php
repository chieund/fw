<?php
/**
 * Created by PhpStorm.
 * User: Chieund
 * Date: 9/7/2015
 * Time: 3:11 PM
 */

/* Show display errors */
ini_set('display_errors', TRUE);
error_reporting(E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_USER_ERROR);

/* Get url root */
define('PG_ROOT', realpath(dirname(__FILE__)));

/* config */
include "config.php";

/* include lib */
include "lib/registry.php";
include "lib/DBDriver.php";
include "lib/Controller.php";
include "lib/Model.php";
include "lib/Request.php";
include "lib/Template.php";
include "lib/Session.php";
include "lib/Layout.php";

/* load user */
include "include/class_user.php";

$registry = new Registry();

$session = new Session();
$registry->set('session', $session);

$request = new Request();
$registry->set('request', $request);

$template = new Template();
$registry->set('template', $template);

$layout = new Layout();
$registry->set('layout', $layout);

/* user */
$user = new UserInfo();
$registry->set('userInfo', $user);

/* db */
$database = DBDriver::getInstance();
$database->connectionDB();
$registry->set('database', $database);

$controller = Controller::getInstance();
$controller->dispatch($registry);

